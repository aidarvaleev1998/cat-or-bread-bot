from app import db
from datetime import datetime


class User(db.Model):
    id = db.Column(db.String(40), primary_key=True)
    state = db.Column(db.Enum('IS_SQUARE', 'HAS_EARS', 'END'), nullable=False, default='END')
    reg_date = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)
    msgs = db.relationship('Message', backref='author')


class Message(db.Model):
    id = db.Column(db.Integer(), autoincrement=True, primary_key=True)
    date = db.Column(db.DateTime(), default=datetime.utcnow)
    user_id = db.Column(db.String(40), db.ForeignKey('user.id'))
    sent_by_bot = db.Column(db.Boolean(), default=False)
    text = db.Column(db.TEXT())


class Photo(db.Model):
    id = db.Column(db.Integer(), autoincrement=True, primary_key=True)
    date = db.Column(db.DateTime(), default=datetime.utcnow)
    user_id = db.Column(db.String(40), db.ForeignKey('user.id'))
    link = db.Column(db.String(200))
    label = db.Column(db.Enum('None', 'Cat', 'Bread', 'Both'), nullable=False)
