import pytest
from werkzeug.exceptions import BadRequest

import app
import models


@pytest.fixture
def client():
    app.app.config['TESTING'] = True
    with app.app.test_client() as client:
        yield client


def test_edits1_1(client):
    assert "нет" in app.edits1("не")


def test_edits1_2(client):
    assert "конечно" in app.edits1("кончено")


def test_edits1_3(client):
    assert "да" in app.edits1("да")


def test_edits1_4(client):
    assert "нет" in app.edits1("нетт")


def test_edits1_5(client):
    assert "да" in app.edits1("дв")


def test_is_positive_1(client):
    words = ["да", "конечно", "ага", "пожалуй"]
    assert all([app.is_positive(w) for w in words])


def test_is_positive_2(client):
    words = ["нет", "нет, конечно", "ноуп", "найн"]
    assert not any([app.is_positive(w) for w in words])


def test_is_negative_1(client):
    words = ["нет", "нет, конечно", "ноуп", "найн"]
    assert all([app.is_negative(w) for w in words])


def test_is_negative_2(client):
    words = ["да", "конечно", "ага", "пожалуй"]
    assert not any([app.is_negative(w) for w in words])


def test_get_user_1(client):
    assert app.get_user("test").id == "test"


def test_get_user_2(client):
    user = models.User.query.filter_by(id="test").first()
    if user is not None:
        app.db.session.delete(user)
        app.db.session.commit()
    assert models.User.query.filter_by(id="test").first() is None
    assert app.get_user("test").id == "test"
    assert models.User.query.filter_by(id="test").first() is not None


def test_download_file(client):
    try:
        app.download_file("anime", "tests/image.jpg")
    except BadRequest as e:
        assert str(e) == "400 Bad Request: Wrong image link."


def test_detect_labels_1(client):
    labels = app.detect_labels("tests/test_none.jpg")
    assert "Cat" not in labels and "Bread" not in labels


def test_detect_labels_2(client):
    labels = app.detect_labels("tests/test_cat.jpg")
    assert "Cat" in labels and "Bread" not in labels


def test_detect_labels_3(client):
    labels = app.detect_labels("tests/test_bread.jpg")
    assert "Cat" not in labels and "Bread" in labels


def test_detect_labels_4(client):
    labels = app.detect_labels("tests/test_both.jpg")
    assert "Cat" in labels and "Bread" in labels


def test_detect_labels_5(client):
    try:
        app.detect_labels("tests/broken.jpg")
    except BadRequest as e:
        assert str(e) == "400 Bad Request: Image has invalid format."


def test_start(client):
    rv = client.post('/', data={
        'id': 'test', 'msg': '/start'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Привет! Я помогу отличить кота от хлеба! Объект перед тобой квадратный?"


def test_square(client):
    user = models.User.query.filter_by(id="test").first()
    user.state = "IS_SQUARE"
    app.db.session.add(user)
    app.db.session.commit()
    rv = client.post('/', data={
        'id': 'test', 'msg': 'да'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "У него есть уши?"


def test_not_square(client):
    user = models.User.query.filter_by(id="test").first()
    user.state = "IS_SQUARE"
    app.db.session.add(user)
    app.db.session.commit()
    rv = client.post('/', data={
        'id': 'test', 'msg': 'нет'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Это кот, а не хлеб! Не ешь его!"


def test_has_ears(client):
    user = models.User.query.filter_by(id="test").first()
    user.state = "HAS_EARS"
    app.db.session.add(user)
    app.db.session.commit()
    rv = client.post('/', data={
        'id': 'test', 'msg': 'да'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Это кот, а не хлеб! Не ешь его!"


def test_has_no_ears(client):
    user = models.User.query.filter_by(id="test").first()
    user.state = "HAS_EARS"
    app.db.session.add(user)
    app.db.session.commit()
    rv = client.post('/', data={
        'id': 'test', 'msg': 'нет'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Это хлеб, а не кот! Ешь его!"


def test_bla(client):
    user = models.User.query.filter_by(id="test").first()
    user.state = "HAS_EARS"
    app.db.session.add(user)
    app.db.session.commit()
    rv = client.post('/', data={
        'id': 'test', 'msg': 'блабла'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Простите, я вас не понял("


def test_recognize(client):
    rv = client.post('/recognize', data={
        'id': 'test', 'link': 'https://imageup.ru/img104/3459166/koshak-prikolnyy-zelenye-45a5d49.jpg'
    })
    json_data = rv.get_json()
    assert json_data['msg'] == "Я вижу и хлеб, и кота!"
