import os
import boto3
import requests

from flask import Flask, abort
from flask_restplus import Resource, Api, reqparse
from flask_sqlalchemy import SQLAlchemy
from requests.exceptions import MissingSchema

BUCKET_NAME = os.environ["BUCKET_NAME"]

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

import models

db.create_all()

api = Api(
    app=app,
    version='1.0',
    description='API',
    doc='/api/',
    default='mapi',
    default_label='API'
)


def edits1(word):
    """
    Generate a set of words, which are 1 edit distance away from the given one
    :param word: a word to apply the edits
    :return: the generated set of words
    """
    letters = ' ,абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [L + R[1:] for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R) > 1]
    replaces = [L + c + R[1:] for L, R in splits if R for c in letters]
    inserts = [L + c + R for L, R in splits for c in letters]
    return set(deletes + transposes + replaces + inserts)


def is_positive(msg):
    """
    Is the given message positive
    :param msg: a message
    :return: True if positive, False otherwise
    """
    words = {"да", "конечно", "ага", "пожалуй"}
    return len(edits1(msg.lower()).intersection(words)) > 0


def is_negative(msg):
    """
    Is the given message negative
    :param msg: a message
    :return: True if negative, False otherwise
    """
    words = {"нет", "нет, конечно", "ноуп", "найн"}
    return len(edits1(msg.lower()).intersection(words)) > 0


def get_user(id):
    """
    Get the User object given his id
    :param id: a user id
    :return: the User object from database
    """
    user = models.User.query.filter_by(id=id).first()
    if not user:
        user = models.User(id=id)
        db.session.add(user)
        db.session.commit()
    return user


@api.route('/')
class CatOrBreadBot(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str, help='User id')
        parser.add_argument('msg', type=str, help='Message')
        args = parser.parse_args()

        user = get_user(args['id'])
        msg = args['msg']

        # Save the received message into the database
        db.session.add(models.Message(user_id=user.id, text=msg))

        is_pos, is_neg = is_positive(msg), is_negative(msg)

        if msg == "/start":
            user.state = "IS_SQUARE"
            result = "Привет! Я помогу отличить кота от хлеба! Объект перед тобой квадратный?"
        elif user.state == "END":
            db.session.commit()
            return {"msg": ""}
        elif user.state == "IS_SQUARE" and is_pos and not is_neg:
            user.state = "HAS_EARS"
            result = "У него есть уши?"
        elif user.state == "IS_SQUARE" and is_neg and not is_pos:
            user.state = "END"
            result = "Это кот, а не хлеб! Не ешь его!"
        elif user.state == "HAS_EARS" and is_pos and not is_neg:
            user.state = "END"
            result = "Это кот, а не хлеб! Не ешь его!"
        elif user.state == "HAS_EARS" and is_neg and not is_pos:
            user.state = "END"
            result = "Это хлеб, а не кот! Ешь его!"
        else:
            result = "Простите, я вас не понял("

        # Save the user state
        db.session.add(user)
        # Save the bot's message into the database
        db.session.add(models.Message(user_id=user.id, sent_by_bot=True, text=result))
        db.session.commit()

        return {"msg": result}


def download_file(link, file_path):
    """
    Save a file to the given location from the given link
    :param link: a link to download from
    :param file_path: a file_path to download to
    :return:
    """
    try:
        file = requests.get(link)
        open(file_path, 'wb').write(file.content)
    except MissingSchema:
        abort(400, 'Wrong image link.')


def detect_labels(file_path):
    """
    Detect labels on the image located in the given location using AWS Rekognition
    :param file_path: an image location
    :return: a list of detected labels
    """
    client = boto3.client('s3')
    key = "image"
    bucket = BUCKET_NAME

    response = client.list_buckets()
    buckets = [b['Name'] for b in response['Buckets']]
    if bucket not in buckets:
        abort(500, "AWS Bucket with this name doesn't exist.")

    client.upload_file(file_path, bucket, key)
    try:
        response = boto3.client('rekognition').detect_labels(
            Image={'S3Object': {'Bucket': bucket, 'Name': key}}
        )
    except:
        abort(400, "Image has invalid format.")
    client.delete_object(Bucket=bucket, Key=key)
    return [r["Name"] for r in response["Labels"]]


@api.route('/recognize')
class Recognize(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str, help='User id')
        parser.add_argument('link', type=str, help='Image link')
        args = parser.parse_args()

        user = get_user(args['id'])
        download_file(args['link'], 'image.jpg')
        labels = detect_labels('image.jpg')

        if "Bread" in labels and "Cat" in labels:
            label = "Both"
            result = "Я вижу и хлеб, и кота!"
        elif "Cat" in labels:
            label = "Cat"
            result = "Это кот!"
        elif "Bread" in labels:
            label = "Bread"
            result = "Это хлеб, ешь его!."
        else:
            label = "None"
            result = "Я не вижу ни кота, ни хлеба =("

        # Save a record into the database
        db.session.add(models.Photo(user_id=user.id, link=args['link'], label=label))
        db.session.commit()

        return {"msg": result}


if __name__ == '__main__':
    app.run(host='0.0.0.0')
