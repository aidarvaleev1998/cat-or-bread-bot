tg:@CatOrBreadBot
==================================
A bot that helps you determine if what you see is a cat or bread.

LAUNCH
------------------
Download the repository:

    git clone https://gitlab.com/aidarvaleev1998/cat-or-bread-bot.git

Configure environment variables in `docker-compose.yml`, then run using Docker:

    docker-compose up --build

USAGE
------------------
Try it on Telegram - `@CatOrBreadBot`:
1. `/start` to start a dialogue, it can understand some spell mistakes (up to 1 edit distance)

    `да`, `конечно`, `ага`, `пожалуй` -- for `yes`

    `нет`, `нет, конечно`, `ноуп`, `найн` --  for `no`

2. Send a photo to recognize Cat or Bread

API
------------------
### 1. `/`: 
  * post:
    * Bot starts a dialigue on `/start` message, asks questions and depending on answers determines, what you see.
    * Requires two arguments:
      1. `id`:`str` User id
      2. `msg`:`str` Message
    * If status is success, returns a json with the only field `msg` containing the bot's answer.
### 2. `/recognize`:
  * post:
    * Determines whether there is a cat/bread/both/none on the image given its link and the user id.
    * Requires two arguments:
      1. `id`:`str` User id
      2. `link`:`str` Image link
    * If status is success, returns a json with the only field `msg` containing the bot's answer.

