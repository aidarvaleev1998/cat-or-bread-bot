from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from requests import post
import os

TOKEN = os.environ["BOT_TOKEN"]
SERVER = os.environ["SERVER"]


def handle(username, text):
    return post(f'http://{SERVER}/', data={'id': "tg:" + username, 'msg': text}).json()


def start(bot, update):
    result = handle(update.message.from_user.username, update.message.text)
    update.message.reply_text(result['msg'])


def echo(bot, update):
    result = handle(update.message.from_user.username, update.message.text)
    if result['msg'] != "":
        update.message.reply_text(result['msg'])


def recognize(bot, update):
    result = post(f'http://{SERVER}/recognize', data={'id': "tg:" + update.message.from_user.username,
                                                      'link': update.message.photo[-1].get_file()['file_path']}).json()
    update.message.reply_text(result['msg'])


updater = Updater(TOKEN)
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))
updater.dispatcher.add_handler(MessageHandler(Filters.photo, recognize))
updater.start_polling()
updater.idle()
